const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 

//Solution 1

Object.keys(users).filter(username => {
    const user = users[username];

    //console.log(user.interests);

    let interestOfVideoGames;
    const value = "Video Games";
    if(user.interests !== undefined)
    {
         interestOfVideoGames = user.interests.join(",");
    }
    else{
        interestOfVideoGames = user.interest.join(",");
    }

if(interestOfVideoGames.includes(value))
{
    console.log(user);
};
 
});


console.log('--------------------------------------')

//Solution 2

Object.keys(users).filter(username => {
    const user = users[username];

    if(user.nationality==="Germany")
    {
        console.log(user);
    }
});


console.log('--------------------------------------')

// Solution 3   not working

// Object.values(users).sort((userA,userB)=>{

//     let LevelA;
//     let LevelB;
//     let a=userA.desgination.toString();
//     let b=userB.desgination.toString();
//    // console.log(a.includes("Senior"));
//     if(a.includes("Senior"))
//     {
//         LevelA=3;
//     }
//     else if(a.includes("Developer"))
//     {
//         LevelA=2;
//     }
//     else{
//         LevelA=1;
//     }

//     if(b.includes("Senior"))
//     {
//         LevelB=3;
//     }
//     else if(b.includes("Developer"))
//     {
//         LevelB=2;
//     }
//     else{
//         LevelB=1;
//     }
    

//     if(LevelA !== LevelB)
//     {
//         return LevelB-LevelA
//     }
//     else{
//     return userB.age-userA.age;
//     }
// })

// console.log(users);

console.log("-----------------------------------------")

//Solution 4

Object.keys(users).filter(username => {
    const user = users[username];

    if(user.qualification==="Masters")
    {
        console.log(user);
    }
});